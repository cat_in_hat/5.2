resource "google_compute_firewall" "default" {
  name    = "firewall-1"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "one_host" {
  name    = "firewall-2"
  network = "default"
  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
  }

  source_ranges = ["10.0.0.23/32"]
}
